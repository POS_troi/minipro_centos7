minipro for CentOS7 x86_64
========
An opensource rewrittement of autoelectric.cn programming utility for CentOS7 x86_64

[Original project for the Debian OS](https://github.com/vdudouyt/minipro)

## Features
* Compatibility with Minipro TL866CS and Minipro TL866A
* More than 13000 target devices (including AVRs, PICs, various BIOSes and EEPROMs)
* ZIF40 socket and ISP support
* Vendor-specific MCU configuration bits
* Chip ID verification
* Overcurrency protection
* System testing

## Synopsis

```nohighlight
$ minipro -p ATMEGA48 -w atmega48.bin
$ minipro -p ATMEGA48 -r atmega48.bin
```

## Installing 

```nohighlight
sudo yum install libusbx libusbx-dev git
git clone https://github.com/POStroi/minipro_centos7.git && cd minipro_centos7/
make
sudo make install
sudo udevadm control --reload-rules
```
